const words = require('./words.json').words;
const permut = require('./permut');

const findMeaningful = (characters, word_length) => {
  word_length == word_length || characters.length;
  const permutations = permut(characters, word_length).filter(
    i => i.length > 1
  );
  return words
    .filter(w => permutations.includes(w))
    .sort((a, b) => a.length - b.length);
};

const start = Date.now();
console.log(findMeaningful(process.argv[2], process.argv[3]));
console.log(Date.now() - start, words.length);
