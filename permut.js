function swap(array, i, j) {
  if (i != j) {
    var swap = array[i];
    array[i] = array[j];
    array[j] = swap;
  }
}

function permute_rec(res, str, array) {
  if (array.length == 0) {
    res.push(str);
  } else {
    for (var i = 0; i < array.length; i++) {
      swap(array, 0, i);
      permute_rec(res, str + array[0], array.slice(1));
      swap(array, 0, i);
    }
  }
}

function permute(array) {
  var res = [];

  permute_rec(res, '', array);
  return res;
}

function xpermute_rec(res, sub, array, word_length) {
  if (array.length == 0) {
    if (sub.length > 0 && sub.length == word_length) permute_rec(res, '', sub);
  } else {
    xpermute_rec(res, sub, array.slice(1), word_length);
    xpermute_rec(res, sub.concat(array[0]), array.slice(1), word_length);
  }
}

module.exports = function permute__of_all_size(array, word_length) {
  var res = [];
  xpermute_rec(res, [], array, word_length);
  return res;
};
